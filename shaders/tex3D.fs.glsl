#version 300 es

precision mediump float;

in vec2 vTexCoords; // Coordonnées de texture du sommets

out vec3 fFragColor;

uniform sampler2D texture_diffuse1;

void main(){
  fFragColor = vec3(texture(texture_diffuse1,vTexCoords).rgb);

}