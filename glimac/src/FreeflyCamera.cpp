#include <cmath>
#include <vector>
#include <iostream>
#include <glimac/glm.hpp>
#include "glimac/FreeflyCamera.hpp"

// Constructeur
FreeflyCamera::FreeflyCamera(){
  this->m_Position = glm::vec3(0.0,0.0,0.0);
  this->m_fPhi = glm::pi<float>();
  this->m_fTheta = 0.0;
  computeDirectionVectors();
};

//Methode
void FreeflyCamera::computeDirectionVectors(){
  this->m_FrontVector = glm::vec3(glm::cos(this->m_fTheta)*glm::sin(this->m_fPhi),glm::sin(this->m_fTheta), glm::cos(this->m_fTheta)*glm::cos(this->m_fPhi));
  this->m_LeftVector = glm::vec3(glm::sin(this->m_fPhi + glm::pi<float>()/2.0),0,glm::cos(this->m_fPhi + glm::pi<float>()/2.0));
  this->m_UpVector = glm::cross(this->m_FrontVector,this->m_LeftVector);
};

void FreeflyCamera::moveLeft(float t){
  this->m_Position += t*this->m_LeftVector;
};

void FreeflyCamera::moveFront(float t){
  this->m_Position += t*this->m_FrontVector;
};

void FreeflyCamera::rotateLeft(float degrees){
  this->m_fTheta += degrees*(glm::pi<float>()/180.0);
  computeDirectionVectors();
};

void FreeflyCamera::rotateUp(float degrees){
  this->m_fPhi += degrees*(glm::pi<float>()/180.0);
  computeDirectionVectors();
};

glm::mat4 FreeflyCamera::getViewMatrix() const{
  glm::mat4 ViewMatrix = glm::lookAt(this->m_Position, this->m_Position + this->m_FrontVector,this->m_UpVector);
  return(ViewMatrix);
};
