#include <cmath>
#include <vector>
#include <iostream>
#include <glimac/glm.hpp>
#include "glimac/TrackballCamera.hpp"

// Constructeur
TrackballCamera::TrackballCamera()
: m_fDistance(5), m_fAngleX(0), m_fAngleY(0){};

//Methode

void TrackballCamera::moveFront(float alpha){
  this->m_fDistance +=alpha;
};

void TrackballCamera::rotateLeft(float degrees){
  this->m_fAngleX +=degrees;
};

void TrackballCamera::rotateUp(float degrees){
  this->m_fAngleY +=degrees;
};

glm::mat4 TrackballCamera::getViewMatrix() const{
  glm::mat4 ViewMatrix;
  ViewMatrix =  glm::translate(glm::mat4(1.0), glm::vec3(0,0,-this->m_fDistance));
  ViewMatrix =  glm::rotate(ViewMatrix, this->m_fAngleX , glm::vec3(1.0,0.0,0.0));
  ViewMatrix =  glm::rotate(ViewMatrix, this->m_fAngleY , glm::vec3(0.0,1.0,0.0));
  return(ViewMatrix);
};
