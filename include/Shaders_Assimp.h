#ifndef JEU_SHADERS_ASSIMP_H
#define JEU_SHADERS_ASSIMP_H

#include <GL/glew.h>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shaders_Assimp {
public:
    unsigned int ID;

    Shaders_Assimp(const char* vertexPath, const char* fragmentPath);

    inline void use(){
        glUseProgram(ID);
    };

    inline void setBool(const std::string &name, bool value) const
    {
        glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
    }

    inline void setInt(const std::string &name, int value) const
    {
        glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
    }

    inline void setFloat(const std::string &name, float value) const
    {
        glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
    }

private:
    void checkCompileErrors(unsigned int shader, std::string type);
};


#endif //JEU_SHADERS_ASSIMP_H
