#include "Object.h"

#ifndef JEU_FILE_H
#define JEU_FILE_H


class File : public Object {


public:

    File();

    ~File();

    void interact() override;
    bool isFounded() const;

};


#endif //JEU_FILE_H
