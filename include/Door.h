#include "Object.h"

#ifndef MAIN_CPP_DOOR_H
#define MAIN_CPP_DOOR_H


class Door : public Object {

public:
    void interact() override;
};


#endif //MAIN_CPP_DOOR_H
