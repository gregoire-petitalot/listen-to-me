#pragma once

#include <iostream>

struct Texture {
    unsigned int Id;
    std::string type;
    std::string path;
};
