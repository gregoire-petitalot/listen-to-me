#ifndef JEU_WINDOWS_H
#define JEU_WINDOWS_H

#include <glimac/SDLWindowManager.hpp>
#include <glimac/FreeflyCamera.hpp>
#include <GL/glew.h>
#include <iostream>

using namespace glimac;

class Windows {
public:
    FreeflyCamera camera;
    SDLWindowManager windowManager;

    Windows();
    ~Windows();
};


#endif //JEU_WINDOWS_H
