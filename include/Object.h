#ifndef JEU_OBJECT_H
#define JEU_OBJECT_H

#include "glimac/glm.hpp"
#include "Model.h"

class Object {

public:


    Object();

    ~Object();

    virtual void interact();

    bool isInteractive() const;

    bool isMovable() const;

    void setInteractive(bool isInteractive);

    void setMovable(bool isMovable);

    void setCoordinates(const glm::vec4 &coordinates);

    glm::vec4 getCoordinates();

    void setModelMatrix(const glm::mat4 &matrix);

    glm::mat4 getModelMatrix();

    bool isFounded() const;

    bool isMoved() const;

protected:

    glm::vec4 m_coordinates;
    glm::mat4 m_ModelMatrix;
    bool m_isInteractive;
    bool m_isMovable;
    bool m_isFounded;
    bool m_isMoved;
};


#endif //JEU_OBJECT_H
