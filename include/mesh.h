#ifndef JEU_MESH_H
#define JEU_MESH_H

#include "Texture.h"
#include <glimac/common.hpp>
#include <vector>
#include <GL/glew.h>
#include "Shaders_Assimp.h"

using namespace glimac;

class mesh{
public:
    std::vector<ShapeVertex> Vertices;
    std::vector<unsigned int> Indices;
    std::vector<Texture> Textures;

    mesh(std::vector<ShapeVertex> NewVertices, std::vector<unsigned int> NewIndices, std::vector<Texture> NewTextures);
    ~mesh();

    void Draw(Shaders_Assimp  &shader);

private:
    unsigned int VAO;
    unsigned int VBO;
    unsigned int EBO;

    void SetUpMesh();
};


#endif //JEU_MESH_H
