
#include "../include/SoundManager.h"

std::vector<Sound *> SoundManager::s_tabSound;
Mix_Music *SoundManager::s_ambiance;
int SoundManager::CHANNELS;


SoundManager::SoundManager() {}

SoundManager::~SoundManager() {}

void SoundManager::init(const char *room) {
    try {
        if (Mix_OpenAudio(SOUND_SAMPLING, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) ==
            -1) //Initialisation de l'API Mixer
        {
            throw std::string(Mix_GetError());
        }
        CHANNELS = 10;
        Mix_AllocateChannels(CHANNELS);
        SoundManager::setMusicAmbiance("sounds/ambiance-1.wav", -1);
        char fullPath[100] = "sounds/";
        strcat(fullPath, room);
        SoundManager::loadSounds(fullPath);
    }
    catch (const std::string &error) {
        std::cerr << error << std::endl;
    }
}

void SoundManager::close() {
    Mix_FreeMusic(SoundManager::s_ambiance);
    Mix_HaltChannel(-1);
    for (int i = 0; i < SoundManager::s_tabSound.size(); ++i) {
        try {
            if (SoundManager::s_tabSound.at(i) == nullptr) {
                throw std::string("erreur: close() pas de fichier a l'indice :") + (char) i;
            }
            Mix_FreeChunk(SoundManager::s_tabSound.at(i)->_soundNoise);
        }
        catch (const std::string &error) {
            std::cerr << error << std::endl;
        }
    }
    Mix_CloseAudio();
}


void SoundManager::setMusicAmbiance(const char *filePath, const int volume) {
    try {
        SoundManager::s_ambiance = Mix_LoadMUS(filePath);
        if (s_ambiance == nullptr) {
            throw std::string("erreur lors du chargement de la musique d'ambiance :") + filePath;
        }
        if (Mix_PlayMusic(SoundManager::s_ambiance, -1) == -1) {
            throw std::string(Mix_GetError()) + "------ du " + filePath;
        }
    }
    catch (const std::string &error) {
        std::cerr << error << std::endl;
    }
}

void SoundManager::loadSounds(const char *filePath) {
    DIR *directory = nullptr;
    try {
        directory = opendir(filePath); // ouverture dossier

        if (directory == nullptr) {
            throw std::string("erreur lors de l'ouverture du dossier:") + filePath;
        }

        struct dirent *file = nullptr;
        int num = 0;
        char fullPath[100];
        while ((file = readdir(directory)) != nullptr) {
            if (strcmp(file->d_name, ".") != 0 && // le fichier doit ni être "." ni ".."
                strcmp(file->d_name, "..") != 0) {
                num++;
                if (num > CHANNELS) {
                    CHANNELS = CHANNELS * 2;
                    Mix_AllocateChannels(CHANNELS);
                }

                strcpy(fullPath, filePath);
                strcat(fullPath, "/");
                strcat(fullPath, file->d_name);
                try {
                    loadOneSound(file, fullPath);
                }
                catch (const std::string &error) {
                    std::cerr << error << std::endl;
                }
            }
        }
    }
    catch (const std::string &error) {
        std::cerr << error << std::endl;
    }
}

void SoundManager::loadOneSound(dirent *&file, const char *fullPath) {
    Sound *tps = new Sound();
    strcpy(tps->_soundName, file->d_name);
    tps->_soundNoise = Mix_LoadWAV(fullPath);

    if (tps->_soundNoise == nullptr) {
        throw std::string("erreur lors de le chargement de :") + file->d_name;
    }
    SoundManager::s_tabSound.push_back(tps);
}

Sound *SoundManager::getSound(const char *soundToPlayName) {
    for (int i = 0; i < s_tabSound.size(); ++i) {
        if (strcmp(s_tabSound.at(i)->_soundName, soundToPlayName) == 0) {
            return s_tabSound.at(i);
        }
    }
    return nullptr;
}

void SoundManager::playSound(const char *soundToPlayName) {
    try {
        Sound *soundToPlay = getSound(soundToPlayName);
        if (soundToPlay == nullptr) {
            throw std::string("erreur: le nom n'existe pas:") + soundToPlayName;
        }
        if (soundToPlay->_soundNoise == nullptr) {
            throw std::string("erreur: le bruit du son n'est pas alloe de:") + soundToPlayName;
        }
        int chosenChannel = channelToPlay(soundToPlay->_soundNoise);
        if (chosenChannel != -1) {
            Mix_PlayChannel(chosenChannel, soundToPlay->_soundNoise, 0);
        }
    }
    catch (const std::string &error) {
        std::cerr << error << std::endl;
    }


}

int SoundManager::channelToPlay(const Mix_Chunk *sound) {
    if (SoundManager::isPlaying(sound)) {
        return -1;
    }

    int channel = SoundManager::findFreeChannel();
    if (channel != -1) {
        return channel;
    }

    return CHANNELS - 1;
}

bool SoundManager::isPlaying(const Mix_Chunk *sound) {
    for (int i = 0; i < CHANNELS; ++i) {
        if (Mix_GetChunk(i) == sound && Mix_Playing(i)) {
            return true;
        }
    }
    return false;
}

int SoundManager::findFreeChannel() {
    for (int i = 0; i < CHANNELS; ++i) {
        if (!Mix_Playing(i)) {
            return i;
        }
    }
    return -1;
}
