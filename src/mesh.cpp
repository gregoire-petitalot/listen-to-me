#include "../include/mesh.h"

mesh::mesh(std::vector<ShapeVertex> NewVertices, std::vector<unsigned int> NewIndices, std::vector<Texture> NewTextures)
: Vertices(NewVertices), Indices(NewIndices), Textures(NewTextures) {
    SetUpMesh();
};

mesh::~mesh(){};

void mesh::Draw(Shaders_Assimp &shader){
    unsigned int diffuseNr=1;
    unsigned int specularNr=1;

    for(unsigned int i=0; i<Textures.size(); i++){
        glActiveTexture(GL_TEXTURE0 +i);

        std::string number;
        std::string name = Textures[i].type;
        if(name=="texture_diffuse"){
            number = std::to_string(diffuseNr++);
        }else if(name=="texture_specular"){
            number = std::to_string(specularNr++);
        }
        shader.setFloat(("material." + name+number).c_str(),i);
        glBindTexture(GL_TEXTURE_2D, Textures[i].Id);
    }
    glActiveTexture(GL_TEXTURE0);

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, Indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
};

void mesh::SetUpMesh(){
    glGenVertexArrays(1,&VAO);
    glGenBuffers(1,&VBO);
    glGenBuffers(1,&EBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, Vertices.size()* sizeof(ShapeVertex), &Vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices.size() * sizeof(unsigned int), &Indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (GLvoid*)(offsetof(ShapeVertex,position)));

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1,3,GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (GLvoid*)(offsetof(ShapeVertex,normal)));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2,2,GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (GLvoid*)(offsetof(ShapeVertex,texCoords)));

    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);
};
