#include "../include/Box.h"

Box::Box() : Object() {
    this->m_isMovable = true;
    this->m_isInteractive = true;
    this->m_isMoved = false;
}

void Box::interact() {
    if(m_isMovable){
        if( isMoved()) this->m_isMoved = false;
        else this->m_isMoved = true;
    }
}

Box::~Box() = default;
