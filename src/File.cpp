#include "../include/File.h"

File::File() : Object(){
    this->m_isMovable = false;
    this->m_isInteractive = true;
}

void File::interact() {
    this->m_isFounded = true;
}

bool File::isFounded() const {
    return this->m_isFounded;
}

File::~File() = default;
