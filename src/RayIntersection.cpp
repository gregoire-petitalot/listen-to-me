#include "../include/RayIntersection.h"

RayIntersection::RayIntersection(float screenWidth, float screenHeight)
        : screenHeight(screenHeight), screenWidth(screenWidth), mouseX(0), mouseY(0) {

};

RayIntersection::~RayIntersection() {};

void RayIntersection::setRayDirectionAndOrigin() {
    glm::vec4 RayStart(
            (this->mouseX / this->screenWidth - 0.5f) * 2.0f,
            (this->mouseY / this->screenWidth - 0.5f) * 2.0f,
            -1.0, 1.0f
    );

    glm::vec4 RayEnd(
            (this->mouseX / this->screenWidth - 0.5f) * 2.0f,
            (this->mouseY / this->screenWidth - 0.5f) * 2.0f,
            0.0, 1.0f
    );

    glm::mat4 InverseMatrix = glm::inverse(this->ProjectionMatrix * this->ViewMatrix);

    glm::vec4 RayStartGlobal = InverseMatrix * RayStart;
    glm::vec4 RayEndGlobal = InverseMatrix * RayEnd;

    glm::vec3 RayDirection(RayEndGlobal - RayStartGlobal);
    RayDirection = glm::normalize(RayDirection);

    this->rayOrigin = glm::vec3(RayStartGlobal);
    this->rayDirection = glm::vec3(RayDirection);
};

bool RayIntersection::TestRayIntersectionXaxis(int indice) {
    float tMin = 0.0f;
    float tMax;

    glm::vec3 OBBPosition_World(this->ModelMatrix[3].x, this->ModelMatrix[3].y, this->ModelMatrix[3].z);
    glm::vec3 Difference = OBBPosition_World - this->rayOrigin;


    glm::vec3 Xaxis(this->ModelMatrix[0].x, this->ModelMatrix[0].y, this->ModelMatrix[0].z);
    float DotXaxisDifference = glm::dot(Xaxis, Difference);
    float DotRayDirectionXaxis = glm::dot(this->rayDirection, Xaxis);

    glm::vec4 CoordonneeMin = ObjectsManager::objets[indice]->getCoordinates() + glm::vec4(0.5,0.5,0.5,0.0);
    glm::vec4 CoordonneeMax = ObjectsManager::objets[indice]->getCoordinates() + glm::vec4(-0.5,-0.5,-0.5,0.0);

    if (fabs(DotRayDirectionXaxis) > 0.001f) {
        // Distance entre le rayon origin et le plan d'intersection
        float DistanceWithLeftPlan = (DotXaxisDifference + CoordonneeMin.x ) / DotRayDirectionXaxis;
        float DistanceWithRightPlan = (DotXaxisDifference + CoordonneeMax.x) / DotRayDirectionXaxis;

        // On considere que le plan de gauche et le plus proche de la camera
        if (DistanceWithLeftPlan > DistanceWithRightPlan) {
            float temp = DistanceWithLeftPlan;
            DistanceWithLeftPlan = DistanceWithRightPlan;
            DistanceWithRightPlan = temp;
        }

        if (DistanceWithRightPlan < tMax) {
            tMax = DistanceWithRightPlan;
        }

        if (DistanceWithLeftPlan < tMin) {
            tMin = DistanceWithLeftPlan;
        }

        if (tMax < tMin) {
            return false;
        }
    } else {
        if((-DotXaxisDifference + CoordonneeMin.x) > 0.0f || (-DotXaxisDifference + CoordonneeMax.x )<0.0f ){
            return false;
        }
    }
    return true;
};

bool RayIntersection::TestRayIntersectionYaxis(int indice) {
    float tMin = 0.0f;
    float tMax;

    glm::vec3 OBBPosition_World(this->ModelMatrix[3].x, this->ModelMatrix[3].y, this->ModelMatrix[3].z);
    glm::vec3 Difference = OBBPosition_World - this->rayOrigin;


    glm::vec3 Yaxis(this->ModelMatrix[1].x, this->ModelMatrix[1].y, this->ModelMatrix[1].z);
    float DotXaxisDifference = glm::dot(Yaxis, Difference);
    float DotRayDirectionXaxis = glm::dot(this->rayDirection, Yaxis);

    glm::vec4 CoordonneeMin = ObjectsManager::objets[indice]->getCoordinates() + glm::vec4(0.5,0.5,0.5,0.0);
    glm::vec4 CoordonneeMax = ObjectsManager::objets[indice]->getCoordinates() + glm::vec4(-0.5,-0.5,-0.5,0.0);

    if (fabs(DotRayDirectionXaxis) > 0.001f) {
        // Distance entre le rayon origin et le plan d'intersection
        float DistanceWithLeftPlan = (DotXaxisDifference + CoordonneeMin.y) / DotRayDirectionXaxis;
        float DistanceWithRightPlan = (DotXaxisDifference + CoordonneeMax.y ) / DotRayDirectionXaxis;

        // On considere que le plan de gauche et le plus proche de la camera
        if (DistanceWithLeftPlan > DistanceWithRightPlan) {
            float temp = DistanceWithLeftPlan;
            DistanceWithLeftPlan = DistanceWithRightPlan;
            DistanceWithRightPlan = temp;
        }

        if (DistanceWithRightPlan < tMax) {
            tMax = DistanceWithRightPlan;
        }

        if (DistanceWithLeftPlan < tMin) {
            tMin = DistanceWithLeftPlan;
        }

        if (tMax < tMin) {
            return false;
        }
    } else {
        if((-DotXaxisDifference + CoordonneeMin.y) > 0.0f || (-DotXaxisDifference + CoordonneeMax.y) <0.0f ){
            return false;
        }
    }
    return true;
};

int RayIntersection::TestRayIntersectionWithObj() {
    int index = -1;
    float distance;
    float distanceMax = 10.0;

    for(int i=0;i<ObjectsManager::objets.size();i++) {
        this->setModelMatrix(ObjectsManager::objets[i]->getModelMatrix());
        if (TestRayIntersectionXaxis(i) && TestRayIntersectionYaxis(i)) {
            float distanceTemp = glm::distance(glm::vec3(ObjectsManager::objets[i]->getCoordinates()),this->cameraCoordinate);
            if (distanceTemp < distance && distance < distanceMax) {
                distance = distanceTemp;
                index = i;
            }
        }
    }
    return index;
};

int RayIntersection::TestRayIntersectionWithObj_2(){
    int index = -1;
    int distance;
    int distanceMax=4;

    for(int i=0;i<ObjectsManager::objets.size();i++) {
        float distanceTemp = glm::distance(glm::vec3(ObjectsManager::objets[i]->getCoordinates()),this->cameraCoordinate);
        if (distanceTemp < distanceMax) {
            distance = distanceTemp;
            index = i;
        }
    }
    return index;
}