#include "../include/Windows.h"

Windows::Windows() : windowManager(0,0,"ListenToMe",SDL_FULLSCREEN) {
    // Initialize glew for OpenGL3+ support
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
    }

    SDL_EnableKeyRepeat(10,10);
    SDL_WM_GrabInput(SDL_GRAB_ON);
};

Windows::~Windows(){};